<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register(){
        return view('register');
    }

    public function post(Request $request){

        $nama_dpn = $request->nama_dpn;
        $nama_blkng = $request->nama_blkng;
        return view('welcome',\compact('nama_dpn','nama_blkng'));
    }

    public function welcome(){
        return view('welcome');
    }
}
